
# NeoCraft

![logo](NeoCraft.png)

[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/gluten-free.svg)](https://forthebadge.com)

NeoCraft is a "Vanilla+"-style Minecraft modpack that aims to provide a better base experience through performance enhancements, polish, and tweaks, without changing any game mechanics or adding any significant new content.

NeoCraft uses the Fabric mod loader and every mod has been carefully examined and tweaked to ensure cohesion and consistency.

Some features include:

* YUNG's Better Caves + Cave Biomes to enhance the caving experience
* Canvas Renderer which improves performance and introduces some lightweight shaders which vastly improve the visuals of the game
* A ton of performance mods - FPS and TPS with NeoCraft is *much* better and more stable than Vanilla
* Various user interface improvements to make the game more polished and easy to use
* Miscellaneous tweaks to address pain points and frustrating aspects of the game

You can use NeoCraft as the base for another modpack, or just play it on its own!
